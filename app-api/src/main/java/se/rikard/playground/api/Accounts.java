package se.rikard.playground.api;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Accounts {

	@XmlElement(name = "account")
	private List<Account> accountList;

	public List<Account> getAccounts() {
		return accountList;
	}

	public void setAccounts(List<Account> accountList) {
		this.accountList = accountList;
	}
}
