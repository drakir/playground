package se.rikard.playground.client;

import com.google.gwt.i18n.client.Messages;

public interface AppMessages extends Messages {

	String sendButton();

	String nameField();

}
