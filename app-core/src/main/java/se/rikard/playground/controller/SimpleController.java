package se.rikard.playground.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.rikard.playground.api.Account;
import se.rikard.playground.api.Accounts;

@Controller
public class SimpleController {

	@RequestMapping(value = "account/{accountId}", method = RequestMethod.GET)
	public @ResponseBody
	Account getAccountById(@PathVariable("accountId") long accountId) {

		return new Account(accountId, "Personkonto", 100.00);
	}

	@RequestMapping(value = "accounts", method = RequestMethod.GET)
	public @ResponseBody
	Accounts getAccounts() {

		List<Account> accounts = Arrays.asList(new Account(1l, "Personkonto", 100.00), new Account(2l, "Sparkonto",
				55400.00));

		Accounts accountsWrapper = new Accounts();
		accountsWrapper.setAccounts(accounts);
		return accountsWrapper;
	}

}
