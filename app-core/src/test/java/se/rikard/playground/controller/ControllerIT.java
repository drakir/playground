package se.rikard.playground.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.web.client.RestTemplate;

import se.rikard.playground.api.Account;
import se.rikard.playground.api.Accounts;

public class ControllerIT {
	private static final String APPLICATION_XML = "application/xml";
	private static final String APPLICATION_JSON = "application/json";
	private ClientHttpRequestInterceptor acceptHeaderXML;
	private ClientHttpRequestInterceptor acceptHeaderJSON;
	private RestTemplate restTemplate;

	@Before
	public void setUp() {
		acceptHeaderXML = new AcceptHeaderHttpRequestInterceptor(APPLICATION_XML);
		acceptHeaderJSON = new AcceptHeaderHttpRequestInterceptor(APPLICATION_JSON);
		restTemplate = new RestTemplate();
	}

	@Test
	public void shouldRetrieveAccountAsXML() {
		restTemplate.setInterceptors(Arrays.asList(acceptHeaderXML));
		long accountId = 1;
		Account account = restTemplate.getForObject("http://localhost:8080/app-core/rest/account/{accountId}",
				Account.class, accountId);
		assertEquals(accountId, account.getAccountId());
	}

	@Test
	public void shouldRetrieveAccountsAsXML() {
		restTemplate.setInterceptors(Arrays.asList(acceptHeaderXML));
		Accounts accounts = restTemplate.getForObject("http://localhost:8080/app-core/rest/accounts", Accounts.class);
		assertNotNull(accounts);
	}

	@Test
	public void shouldRetrieveAccountAsJSON() {
		restTemplate.setInterceptors(Arrays.asList(acceptHeaderJSON));
		long accountId = 1;
		Account account = restTemplate.getForObject("http://localhost:8080/app-core/rest/account/{accountId}",
				Account.class, accountId);
		assertEquals(accountId, account.getAccountId());
	}

	@Test
	public void shouldRetrieveAccountsAsJSON() {
		restTemplate.setInterceptors(Arrays.asList(acceptHeaderJSON));
		Accounts accounts = restTemplate.getForObject("http://localhost:8080/app-core/rest/accounts", Accounts.class);
		assertNotNull(accounts);
	}

	/**
	 * Interceptor to set the Accept header data
	 */
	class AcceptHeaderHttpRequestInterceptor implements ClientHttpRequestInterceptor {
		private String headerValue;

		public AcceptHeaderHttpRequestInterceptor(String headerValue) {
			this.headerValue = headerValue;
		}

		@Override
		public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
				throws IOException {

			HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
			requestWrapper.getHeaders().setAccept(Arrays.asList(MediaType.valueOf(headerValue)));

			return execution.execute(requestWrapper, body);
		}

	}

}
