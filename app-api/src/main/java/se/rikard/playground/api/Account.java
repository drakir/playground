package se.rikard.playground.api;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Account {
	private long accountId;
	private String name;
	private double balance;
	
	/**
	 * Needed because of JAXB
	 */
	public Account() {
	}
	
	public Account(long accountId, String name, double balance) {
		this.accountId = accountId;
		this.name = name;
		this.balance = balance;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
}
